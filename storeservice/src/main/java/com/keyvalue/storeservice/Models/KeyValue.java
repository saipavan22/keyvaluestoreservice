package com.keyvalue.storeservice.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Document(collection = "key_value")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValue implements Serializable {

  @Field("id")
  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  private String id;

  @Field("key")
  private String key;

  @Field("value")
  private String value;

}
