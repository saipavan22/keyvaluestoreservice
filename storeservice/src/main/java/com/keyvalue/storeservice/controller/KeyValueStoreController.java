package com.keyvalue.storeservice.controller;


import com.keyvalue.storeservice.Vo.KeyValueResponseVo;
import com.keyvalue.storeservice.service.KeyValueStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/store-service")
public class KeyValueStoreController {

  @Autowired
  private KeyValueStoreService keyValueStoreService;


  @RequestMapping(value = "/get", method = RequestMethod.GET)
  public KeyValueResponseVo getValue(@RequestParam String key) {
    KeyValueResponseVo responseVo = keyValueStoreService.getValue(key);
    return responseVo;
  }

  @RequestMapping(value = "/add", method = RequestMethod.GET)
  public boolean addKeyValue(@RequestParam String key, @RequestParam String value) {
    return keyValueStoreService.addValue(key, value);
  }

  @RequestMapping(value = "/delete",method = RequestMethod.DELETE)
  public boolean deleteKey(@RequestParam String key){
    return keyValueStoreService.deleteKey(key);
  }

  @RequestMapping(value = "/update", method = RequestMethod.PUT)
  public boolean updateKey(@RequestParam String key, @RequestParam String value) {
    return keyValueStoreService.updateValue(key, value);
  }


}
