package com.keyvalue.storeservice.repository;

import com.keyvalue.storeservice.Models.KeyValue;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface KeyValueRepository extends MongoRepository<KeyValue, String> {
  KeyValue findByKey(String key);

  Long deleteByKey(String key);
}
