package com.keyvalue.storeservice.service;


import com.keyvalue.storeservice.Models.KeyValue;
import com.keyvalue.storeservice.Vo.KeyValueResponseVo;
import com.keyvalue.storeservice.repository.KeyValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;

@Service
public class KeyValueStoreService {

  @Autowired
  private KeyValueRepository keyValueRepository;

  @Autowired
  @Qualifier("keyValueCache")
  private Map<String, String> cacheMap;

  public KeyValueResponseVo getValue(String key) {
    KeyValueResponseVo responseVo = new KeyValueResponseVo();
    String value = null;
    if (cacheMap.containsKey(key)) {
      value = cacheMap.get(key);
    } else {
      KeyValue keyValue = keyValueRepository.findByKey(key);
      value = keyValue.getValue();
      cacheMap.put(key, value);
    }
    responseVo.setValue(value);
    return responseVo;

  }

  public boolean addValue(String key, String value) {
    KeyValue keyValue = new KeyValue();
    keyValue.setKey(key);
    keyValue.setValue(value);
    if (Objects.isNull(keyValueRepository.findByKey(key))) {
      keyValueRepository.save(keyValue);
      return true;
    }
    return false;
  }

  public boolean deleteKey(String key) {
    cacheMap.remove(key);
    return keyValueRepository.deleteByKey(key)==1;
  }

  public boolean updateValue(String key, String value) {

    KeyValue keyValue = keyValueRepository.findByKey(key);
    if (Objects.nonNull(keyValue)) {
      keyValue.setValue(value);
      keyValueRepository.save(keyValue);
      cacheMap.put(key, value);
      return true;
    }
    return false;
  }
}
