package com.keyvalue.storeservice.Vo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class VehicleSearchRequest implements Serializable {

  private static final long serialVersionUID = 1L;
  private PagedRequest pagedRequest;

  private String vehicleType;

  private int startHour;

  private int endHour;

  private String location;

}
