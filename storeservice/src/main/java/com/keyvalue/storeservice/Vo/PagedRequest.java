package com.keyvalue.storeservice.Vo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PagedRequest implements Serializable {

  private static final long serialVersionUID = 1L;

  private List<SortOrder> sort = new ArrayList<SortOrder>();


}

