package com.keyvalue.storeservice.Vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SortOrder {
  private String property = "";
  private String direction = "";

}
