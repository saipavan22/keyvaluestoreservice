package com.keyvalue.storeservice.Vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class VehicleResponseVo {

  private double price;
  private String vehicleType;
  private String vehicleNumber;
  private String location;
  private double distanceTravelled;
}
