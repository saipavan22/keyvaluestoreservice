package com.keyvalue.storeservice.Vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class VehicleBookingRequest {

  private String userId;

  private String vehicleNumber;

  private String startHour;

  private String endHour;
}
